// testing a stepper motor(LDO-28STH-51-0674A) with a LeadShine_DM422C driver module
// on an Uno the onboard led will flash with each step
// this version uses delay() to manage timing
//sw5=on sw6=on


byte XdirectionPin = 9;//pin to control x axis motion
byte XpulsePin = 8;//pin to control pulses
byte YdirectionPin = 11;//pin to control y axis motion...............test
byte YpulsePin = 10;
int numberOfSteps = 500;
int pulseWidthMicros = 20;  // microseconds
int millisbetweenSteps = 1; // milliseconds
int incomingByte=0;
int n = 1;
byte MS1 = 7;
byte MS2 = 6;
byte MS3 = 5;
byte ZpulsePin = 12;
byte ZdirectionPin = 4;
int millisbetweenSteps_Z = 20;//milliseconds
int ledPin = 13;
byte Sleep = 3;//to enable z-axis driver
byte buzzer = A1;

#include <avr/wdt.h>

void setup()
{

  Serial.begin(9600);
  Serial.println("DigitalMicroscopy\n");



  delay(50);
  pinMode(MS1, OUTPUT);
  pinMode(MS2, OUTPUT);
  pinMode(MS3, OUTPUT);
  pinMode(ZpulsePin, OUTPUT);
  pinMode(ZdirectionPin, OUTPUT);
  pinMode(XdirectionPin, OUTPUT);
  pinMode(XpulsePin, OUTPUT);
  pinMode(YdirectionPin, OUTPUT);
  pinMode(YpulsePin, OUTPUT);
  pinMode(ledPin, OUTPUT);
  pinMode(Sleep, OUTPUT);



  digitalWrite(MS1, HIGH); //MICRO STEPPING Z AXIS   1/4
  digitalWrite(MS2, LOW);
  digitalWrite(MS3, LOW);
  digitalWrite(ZpulsePin, LOW);
  digitalWrite(ZdirectionPin, LOW);
  digitalWrite(ledPin, LOW);
  digitalWrite(Sleep, LOW);


}

void loop()
{


  if (Serial.available() > 0)
  {
    incomingByte = Serial.read();

    switch (incomingByte)
    {
      case '1':
        Z_up(7);
        break;
      case '3':
        Z_down(7);
        break;
      case 'r':
        reset();
        break;

      case '4':
        X_fw(1500);
        break;
      case '6':
        X_bw(1500);
        break;
      case '2':
        Y_fw(1500);
        break;
      case '8':
        Y_bw(1500);
        break;
      case 's':
        Serial.println("initiated\n");
        int rows = 0;
        int columns = 0;
        rows = rowentry();
        columns = columnentry();
        int n = imagecount();
        Serial.println("success\n");
        char temp[10];

        while (true) {

          //Serial.println("on");
          if (Serial.available() > 0) {
             incomingByte = Serial.read();
            if (incomingByte == 'a') {
              digitalWrite(buzzer, LOW);
              n = move_motor(n, columns);
              sprintf(temp, "image%d\n", n);
              Serial.println(temp);
              delay(100);
              if (n == (rows * columns)) {
                Serial.println("completed\n");
                break;
              }
            }
            else if (incomingByte == 'e') {
              Serial.println("image\n");
            }
            else if (incomingByte == '3') {
              Z_up(7);
              Serial.println("focus");
              delay(20);
            }
            else if (incomingByte == '1') {
              Z_down(7);
              Serial.println("focus");
              delay(20);
            }
            else if (incomingByte == 'p')
            {
              while (Serial.available() == 0)
              {
                int x = 0;
                while (x < 7)
                {
                  Buzzer();
                  x++;
                }
                delay(2000);
              }


            }



            else if (incomingByte == 'r')
            {
              reset();
            }
          }
        }
        /* for (int i = 1; i < rows; i++)
          {
           while (Serial.read() != 'a') {}
           Serial.println("take image0");
           for (int j = 1; j < columns; j++)
           {

             delay(500);
             while (Serial.read() != 'a') {}
             // Serial.println(j);
             movement(n);
             delay(3000);
             Serial.println("take image");
             delay(3000);

           }


           // while(Serial.read()!='a'){}
           // Serial.println("moving down");
           //delay(2000);
           Y_bw(200);
           delay(3000);
           //Serial.println("take image");
           if (n == 1)
             n = 2;

           else

             n = 1;


          }
          Serial.println("completed");
          break;*/
    }
  }
}

int move_motor(int file, int column) {
  if (file % column == 0) {
    Y_bw(150);
    file++;
    delay(250);
  }
  else if (((file / column) + 1) % 2 == 0) {
    X_fw(200);
    file++;
    delay(250);

  }
  else if (((file / column) + 1) % 2 == 1) {
    X_bw(200);
    file++;
    delay(250);
  }
  return file;
}


void Z_up(int steps_up)
{
  digitalWrite(Sleep, HIGH);
  delay(2);
  digitalWrite(ledPin, HIGH);

  for (int c = 0; c < steps_up; c++)
  {
    //Serial.println(c);

    digitalWrite(ZdirectionPin, LOW);
    digitalWrite(ZpulsePin, HIGH);
    delayMicroseconds(pulseWidthMicros);
    digitalWrite(ZpulsePin, LOW);
    delay(millisbetweenSteps_Z);
  }

  digitalWrite(ledPin, LOW);
  digitalWrite(Sleep, LOW);
}

void Z_down(int steps_up)
{
  digitalWrite(Sleep, HIGH);
  delay(2);
  digitalWrite(ledPin, HIGH);


  for (int f = 0; f < steps_up; f++)
  {
    //Serial.println(f);
    digitalWrite(ZdirectionPin, HIGH);
    digitalWrite(ZpulsePin, HIGH);
    delayMicroseconds(pulseWidthMicros);
    digitalWrite(ZpulsePin, LOW);
    delay(millisbetweenSteps_Z);
  }
  digitalWrite(ledPin, LOW);
  digitalWrite(Sleep, LOW);
}


void X_fw(int steps_left) {
  for (int b = 0; b < steps_left; b++)
  {
    digitalWrite(XdirectionPin, LOW);
    digitalWrite(XpulsePin, HIGH);
    delayMicroseconds(pulseWidthMicros); // this line is probably unnecessary
    digitalWrite(XpulsePin, LOW);

    delay(millisbetweenSteps);
  }

  //digitalWrite(ledPin, !digitalRead(ledPin));
  //Serial.println("left");
}

//  delay(500);


void X_bw(int steps_right) {
  for (int a = 0; a < steps_right; a++)
  {
    //Serial.println("pulse");
    digitalWrite(XdirectionPin, HIGH);
    digitalWrite(XpulsePin, HIGH);
    delayMicroseconds(pulseWidthMicros); // probably not needed
    digitalWrite(XpulsePin, LOW);

    delay(millisbetweenSteps);
  }

  //digitalWrite(ledPin, !digitalRead(ledPin));
  // Serial.println("right");
}


void Y_fw(int steps_down) {
  for (int c = 0; c < steps_down; c++)
  {
    digitalWrite(YdirectionPin, HIGH);
    digitalWrite(YpulsePin, HIGH);
    delayMicroseconds(pulseWidthMicros); // this line is probably unnecessary
    digitalWrite(YpulsePin, LOW);

    delay(millisbetweenSteps);
  }

  //digitalWrite(ledPin, !digitalRead(ledPin));
  //Serial.println("down");
}

//  delay(500);


void Y_bw(int steps_up) {
  for (int d = 0; d < steps_up; d++)
  {
    digitalWrite(YdirectionPin, LOW);
    digitalWrite(YpulsePin, HIGH);
    delayMicroseconds(pulseWidthMicros); // probably not needed
    digitalWrite(YpulsePin, LOW);

    delay(millisbetweenSteps);
  }

  // digitalWrite(ledPin, !digitalRead(ledPin));
  //Serial.println("up");
}





float rowentry()
{
  int n = 0;
  char buffer[] = {' ', ' ', ' ', ' ', ' ', ' ', ' '}; // Receive up to 7 bytes
  Serial.println("rows\n");
first:


  while (!Serial.available()); // Wait for characters
  Serial.readBytesUntil('n', buffer, 7);
  int incomingValue = atoi(buffer);
  if (incomingValue >= 1 && incomingValue <= 400)
  {
    n = incomingValue;
    // Serial.println(n);
    // Serial.println("micro");
    incomingValue = 0;
    return (n);
  }
  else
  {
    Serial.println("rows\n");
    goto first;
  }
}


float columnentry()
{
  int n = 0;
  char buffer[] = {' ', ' ', ' ', ' ', ' ', ' ', ' '}; // Receive up to 7 bytes
  Serial.println("columns\n");
first:


  while (!Serial.available()); // Wait for characters
  Serial.readBytesUntil('n', buffer, 7);
  int incomingValue = atoi(buffer);
  if (incomingValue >= 1 && incomingValue <= 400)
  {
    n = incomingValue;
    // Serial.println(n);
    // Serial.println("micro");
    incomingValue = 0;
    return (n);
  }
  else
  {
    Serial.println("columns\n");
    goto first;
  }
}

void movement(int  n)
{
  switch (n)
  {
    case 1:
      Serial.println("case R");
      X_bw(250);

      break;
    case 2:
      Serial.println("case L");
      X_fw(250);

      break;
  }
}

float imagecount()
{
  int n = 0;
  char buffer[] = {' ', ' ', ' ', ' ', ' ', ' ', ' '}; // Receive up to 7 bytes
  Serial.println("sequence\n");
first:


  while (!Serial.available()); // Wait for characters
  Serial.readBytesUntil('n', buffer, 7);
  int incomingValue = atoi(buffer);
  if (incomingValue >= 1 && incomingValue <= 3000)
  {
    n = incomingValue;
    // Serial.println(n);
    // Serial.println("micro");
    incomingValue = 0;
    return (n);
  }
  else
  {
    Serial.println("sequence\n");
    goto first;
  }
}

void Buzzer()
{



  digitalWrite(buzzer, HIGH);
  delay(100);
  digitalWrite(buzzer, LOW);
  delay(100);



}


void reset()

{
  Serial.println("resetting");
  wdt_enable(WDTO_15MS);
  wdt_reset();
}


